#!/bin/bash

    bash 0-preinstall.sh
    arch-chroot /mnt /root/NoArchNetworcc/1-setup.sh
    source /mnt/root/NoArchNetworcc/install.conf
    arch-chroot /mnt /usr/bin/runuser -u $username -- /home/$username/NoArchNetworcc/2-user.sh
    arch-chroot /mnt /root/NoArchNetworcc/3-post-setup.sh